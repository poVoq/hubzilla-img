# hubzilla-img

Image assets for Hubzilla and RedMatrix (legacy).

* `/hubzilla` — image assets for [hubzilla.org](http://hubzilla.org), extra avatars
* `/redmatrix` — extra RedMatrix icons and badges, red_emoji (a basic emoji addon)

---

## License

©2015 [Iko](https://gitlab.com/u/iko)      
  
Contents are released under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).  
  
For personal use, such as an avatar or profile photo, it is not necessary to attribute/backlink.  
